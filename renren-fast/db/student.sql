-- 系统管理员的表设计
create table `sys_user`
(
    `user_id`        bigint      not null auto_increment,
    `username`       varchar(50) not null comment '用户名',
    `password`       varchar(100) comment '密码',
    `salt`           varchar(20) comment '盐',
    `email`          varchar(100) comment '邮箱',
    `mobile`         varchar(100) comment '手机号',
    `status`         tinyint comment '状态  0：禁用   1：正常',
    `create_user_id` bigint(20) comment '创建者id',
    `create_time`    datetime comment '创建时间',
    primary key (`user_id`),
    unique index (`username`)
) engine = `innodb`
  default character set utf8mb4 comment ='系统用户';

-- 创建学生使用表
use stu_manage;

-- 学生用户的设计
create table `stu_users`
(
    `user_id`        bigint      not null auto_increment comment '学号',
    `username`       varchar(50) not null comment '用户名/学生姓名',
    `password`       varchar(100) comment '密码',
    `salt`           varchar(20) comment '盐',
    `email`          varchar(100) comment '邮箱',
    `mobile`         varchar(100) comment '手机号',
    `status`         tinyint comment '状态  0：禁用   1：正常',
    `create_user_id` bigint(20) comment '创建者id',
    `create_time`    datetime comment '创建时间',
    `yuanxi`         varchar(50) comment '院系',
    `banji`          varchar(50) comment '班级',
    primary key (`user_id`)
) engine = `innodb`
  default character set utf8mb4 comment ='学生';

-- 申请表
create table `stu_shenqin_single`
(
    `id`          bigint not null auto_increment comment '申请表id',
    `user_id`     bigint not null comment '申请人学号',
    `txt`         varchar(1200) comment '申请内容',
    `oss_url`     varchar(1200) comment '申请材料地址',
    `tijiao_time` datetime default null comment '提交时间',
    `shenpi_time` datetime default null comment '审批时间',
    `shenpi_user` varchar(50) comment '审批人',
    `pass`        tinyint comment '是否通过1：通过 0：未通过/待通过',
    `xiangmu_id`  bigint comment '项目类型id',
    primary key (`id`)
) engine = `innodb`
  default character set utf8mb4 comment ='申请表';

-- 项目类型表
create table `stu_xiangmu_type_level`
(
    `id`            bigint      not null auto_increment comment '项目类型id',
    `name`          varchar(50) not null comment '项目名称',
    `use_type`      tinyint     not null comment '是否为可申请项目0：不可申请  1：可申请',
    `ren_type`      tinyint     not null comment '项目最大人数',
    `parent_id`     bigint comment '父级id',
    `level`         int comment '层级',
    `fenshu_single` int comment '单人项目分数',
    `fenshu_group`  int comment '多人项目总分数',
    `fenshu_zhuchi` int comment '多人项目主持分数',
    primary key (`id`)
) engine = `innodb`
  default character set utf8mb4 comment ='申请类型(level型)';

alter table `stu_xiangmu_type_level`
    modify column `ren_type` tinyint comment '注释';

# create table `stu_xiangmu_type_string`
# (
#     `id`       bigint      not null auto_increment comment '项目类型id',
#     `daihao`   varchar(50) not null comment '项目代号',
#     `name`     varchar(50) not null comment '项目名称',
#     `use_type` tinyint     not null comment '是否为可申请项目0：不可申请  1：可申请',
#     `ren_type` tinyint     not null comment '申请类型0单人1多人',
#     primary key (`id`)
# ) engine = `innodb`
#   default character set utf8mb4 comment ='申请类型(字长型)';


-- 添加加分项目的细则
insert into stu_xiangmu_type_level(id, name, use_type, ren_type, parent_id, level, fenshu_single, fenshu_group,
                                   fenshu_zhuchi) value
    (1, '奖学金', 1, 1, 0, 0, 0, 0, 0);
insert into stu_xiangmu_type_level(id, name, use_type, ren_type, parent_id, level, fenshu_single, fenshu_group,
                                   fenshu_zhuchi) value
    (101, '一等奖学金', 1, 1, 1, 1, 10, 0, 0);
insert into stu_xiangmu_type_level(id, name, use_type, ren_type, parent_id, level, fenshu_single, fenshu_group,
                                   fenshu_zhuchi) value
    (102, '二等奖学金', 1, 1, 1, 1, 6, 0, 0);
insert into stu_xiangmu_type_level(id, name, use_type, ren_type, parent_id, level, fenshu_single, fenshu_group,
                                   fenshu_zhuchi) value
    (103, '三等奖学金', 1, 1, 1, 1, 4, 0, 0);
insert into stu_xiangmu_type_level(id, name, use_type, ren_type, parent_id, level, fenshu_single, fenshu_group,
                                   fenshu_zhuchi) value
    (2, '荣誉称号', 0, 1, 0, 0, 0, 0, 0),
    (201, '优秀学生', 1, 1, 2, 1, 3, 0, 0),
    (202, '优秀学生干部', 1, 1, 2, 1, 3, 0, 0),
    (203, '优秀团员', 1, 1, 2, 1, 3, 0, 0),
    (204, '优秀团干部', 1, 1, 2, 1, 3, 0, 0),
    (205, '其他荣誉', 1, 1, 2, 1, 1, 0, 0);

insert into stu_xiangmu_type_level(id, name, use_type, ren_type, parent_id, level, fenshu_single, fenshu_group,
                                   fenshu_zhuchi) value
    (7, '国创项目', 1, 3, 0, 0, 0, 16, 12),
    (8, '新苗项目', 1, 3, 0, 0, 0, 13, 10),
    (9, '校级srt', 0, 3, 0, 0, 0, 0, 0),
    (901, '校级srt(一般)', 1, 3, 9, 1, 0, 10, 6),
    (902, '校级srt(重点)', 1, 3, 9, 1, 0, 8, 5);

insert into stu_xiangmu_type_level(id, name, use_type, ren_type, parent_id, level, fenshu_single, fenshu_group,
                                   fenshu_zhuchi) value
    (3, '学科竞赛', 0, 1, 0, 0, 0, 0, 0),
    (301, 'A类竞赛', 0, 1, 3, 1, 0, 0, 0),
    (30101, '国家级一等奖', 1, 1, 301, 2, 14, 0, 0),
    (30102, '国家级二等奖', 1, 1, 301, 2, 12, 0, 0),
    (30103, '国家级三等奖', 1, 1, 301, 2, 10, 0, 0),
    (30104, '省级一等奖', 1, 1, 301, 2, 9, 0, 0),
    (30105, '省级二等奖', 1, 1, 301, 2, 6, 0, 0),
    (30106, '省级三等奖', 1, 1, 301, 2, 4, 0, 0),
    (30107, '校级一等奖', 1, 1, 301, 2, 3, 0, 0),
    (30108, '校级二等奖', 1, 1, 301, 2, 2, 0, 0),
    (30109, '校级三等奖', 1, 1, 301, 2, 1, 0, 0),
    (302, '非A类竞赛', 0, 1, 3, 1, 0, 0, 0),
    (30201, '国家级一等奖', 1, 1, 302, 2, 14, 0, 0),
    (30202, '国家级二等奖', 1, 1, 302, 2, 12, 0, 0),
    (30203, '国家级三等奖', 1, 1, 302, 2, 10, 0, 0),
    (30204, '省级一等奖', 1, 1, 302, 2, 9, 0, 0),
    (30205, '省级二等奖', 1, 1, 302, 2, 6, 0, 0),
    (30206, '省级三等奖', 1, 1, 302, 2, 4, 0, 0),
    (30207, '校级一等奖', 1, 1, 302, 2, 3, 0, 0),
    (30208, '校级二等奖', 1, 1, 302, 2, 2, 0, 0),
    (30209, '校级三等奖', 1, 1, 302, 2, 1, 0, 0);

insert into stu_users(user_id, username, password, salt, email, mobile, status, create_user_id, create_time, yuanxi,
                      banji) value
    (202245225219, 'linshu', 'linshu', null, 'linshu@qq.com', '123456789', 1, null, now(), '计算机学院', '计算机1801');


-- 创建为多人项目的表逻辑
create table `stu_shenqin_group`
(
    `id`          bigint not null auto_increment comment '申请表id',
    `user_id`     bigint not null comment '申请人(主创)学号',
    `txt`         varchar(1200) comment '申请内容',
    `oss_url`     varchar(1200) comment '申请材料地址',
    `tijiao_time` datetime default null comment '提交时间',
    `shenpi_time` datetime default null comment '审批时间',
    `shenpi_user` varchar(50) comment '审批人',
    `pass`        tinyint comment '是否通过1:通过 0:待通过 -1:未通过',
    `xiangmu_id`  bigint comment '项目类型id',
    primary key (`id`)
) engine = `innodb`
  default character set utf8mb4 comment ='多人项目表';

create table `stu_shenqin_group_students`
(
    `id`        bigint not null auto_increment comment 'id',
    `group_id`  bigint not null comment '多人项目表id',
    `user_id`   bigint not null comment '学生学号',
    `is_leader` tinyint comment '是否为组长 1为组长 0为组员',
    `fenshu`    int comment '所得分数',
    primary key (`id`)
) engine = `innodb`
  default character set utf8mb4 comment ='多人项目关联学生表';


SELECT *
FROM stu_users
WHERE user_id LIKE '2022%';
