package io.renren.modules.stu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 多人项目关联学生表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-18 09:20:08
 */
@Data
@TableName("stu_shenqin_group_students")
public class ShenqinGroupStudentsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 多人项目表id
	 */
	private Long groupId;
	/**
	 * 学生学号
	 */
	private Long userId;
	/**
	 * 是否为组长 1为组长 0为组员
	 */
	private Integer isLeader;
	/**
	 * 所得分数
	 */
	private Integer fenshu;
	/**
	 * 姓名 来源于stu_users表
	 * 未对应表字段
	 */
	@TableField(exist=false) //表示该属性不对应表属性 不会在查询时进行自动装配
	private String stuName;

}
