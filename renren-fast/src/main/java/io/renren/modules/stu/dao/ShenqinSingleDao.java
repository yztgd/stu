package io.renren.modules.stu.dao;

import io.renren.modules.stu.entity.ShenqinSingleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 申请表
 * 
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:08
 */
@Mapper
public interface ShenqinSingleDao extends BaseMapper<ShenqinSingleEntity> {
	
}
