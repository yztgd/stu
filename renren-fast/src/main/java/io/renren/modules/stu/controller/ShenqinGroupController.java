package io.renren.modules.stu.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import io.renren.common.annotation.SysLog;
import io.renren.modules.stu.entity.ShenqinSingleEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.stu.entity.ShenqinGroupEntity;
import io.renren.modules.stu.service.ShenqinGroupService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 多人项目表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-18 09:20:09
 */
@RestController
@RequestMapping("stu/shenqingroup")
@Slf4j
public class ShenqinGroupController {
    @Autowired
    private ShenqinGroupService shenqinGroupService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("stu:shenqingroup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = shenqinGroupService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * 列表
     */
    @RequestMapping("/listWithNameForTeacher")
//    @RequiresPermissions("stu:shenqingroup:list")
    public R listWithNameForTeacher(@RequestParam Map<String, Object> params){
        PageUtils page = shenqinGroupService.listWithNameForTeacher(params);

        return R.ok().put("page", page);
    }
    /**
     * 列表
     */
    @RequestMapping("/listWithName")
//    @RequiresPermissions("stu:shenqingroup:list")
    public R listWithName(@RequestParam Map<String, Object> params){
        PageUtils page = shenqinGroupService.listWithNameForTeacher(params);

        return R.ok().put("page", page);
    }




    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("stu:shenqingroup:info")
    public R info(@PathVariable("id") Long id){
		ShenqinGroupEntity shenqinGroup = shenqinGroupService.getById(id);

        return R.ok().put("shenqinGroup", shenqinGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("stu:shenqingroup:save")
    public R save(@RequestBody ShenqinGroupEntity shenqinGroup){
        shenqinGroup.setTijiaoTime(new Date());
        shenqinGroup.setPass(0);
		shenqinGroupService.save(shenqinGroup);

        return R.ok().put("id", shenqinGroup.getId());
    }

    /**
     * 保存
     */
    @RequestMapping("/updateWithTeacher")
//    @RequiresPermissions("stu:shenqinsingle:save")
    public R updateWithTeacher(@RequestBody ShenqinGroupEntity shenqinGroup){
        log();
        shenqinGroup.setShenpiTime(new Date());
        shenqinGroupService.updateWithTeacher(shenqinGroup);

        return R.ok();
    }

    @SysLog("申报审批")
    public void log(){
        log.debug("申报审批");
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("stu:shenqingroup:update")
    public R update(@RequestBody ShenqinGroupEntity shenqinGroup){
        shenqinGroup.setShenpiTime(new Date());
		shenqinGroupService.updateById(shenqinGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("stu:shenqingroup:delete")
    public R delete(@RequestBody Long[] ids){
		shenqinGroupService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
