package io.renren.modules.stu.dao;

import io.renren.modules.stu.entity.UsersEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学生
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:08
 */
@Mapper
public interface UsersDao extends BaseMapper<UsersEntity> {
	List<UsersEntity> getUsersByNianji(@Param("nianji") String nianji);
}
