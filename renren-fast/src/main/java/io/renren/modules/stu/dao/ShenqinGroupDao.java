package io.renren.modules.stu.dao;

import io.renren.modules.stu.entity.ShenqinGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 多人项目表
 * 
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-18 09:20:09
 */
@Mapper
public interface ShenqinGroupDao extends BaseMapper<ShenqinGroupEntity> {
	
}
