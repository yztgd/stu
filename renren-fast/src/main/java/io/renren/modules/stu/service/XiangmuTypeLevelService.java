package io.renren.modules.stu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.stu.entity.XiangmuTypeLevelEntity;

import java.util.List;
import java.util.Map;

/**
 * 申请类型(level型)
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:07
 */
public interface XiangmuTypeLevelService extends IService<XiangmuTypeLevelEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 添加流式计算代码 返回所有申请项目种类的树形结构
     */
    List<XiangmuTypeLevelEntity> listTreeSingle();
    List<XiangmuTypeLevelEntity> listTreeGroup();
}

