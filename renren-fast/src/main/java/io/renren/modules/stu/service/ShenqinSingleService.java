package io.renren.modules.stu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.stu.entity.ShenqinSingleEntity;

import java.util.Map;

/**
 * 申请表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:08
 */
public interface ShenqinSingleService extends IService<ShenqinSingleEntity> {

    /**
     * 根据参数查询页面数据
     * 此方法用于处理分页查询请求，根据传入的参数对象来获取分页后的数据
     *
     * @param params 查询参数，包含分页及过滤条件
     * @return 分页查询结果，包含查询数据及分页信息
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据用户ID查询页面数据
     * 此方法专用于根据用户ID进行分页查询，仅返回指定用户的相关数据
     *
     * @param params 查询参数，包含分页及过滤条件
     * @param userId 用户ID，用于指定要查询的用户
     * @return 分页查询结果，包含查询数据及分页信息
     */
    public PageUtils pageByUserId(Map<String, Object> params, Long userId);

    /**
     * 查询带有名称的教师列表
     * 此方法用于查询并返回带有名称信息的教师列表，根据传入的参数进行筛选
     *
     * @param params 查询参数，包含分页及过滤条件
     * @return 分页查询结果，包含带有名称信息的教师列表及分页信息
     */
    public PageUtils listWithNameForTeacher(Map<String, Object> params);

    /**
     * 查询带有名称的用户列表
     * 此方法用于查询并返回带有名称信息的用户列表，根据传入的参数及用户ID进行筛选
     *
     * @param params 查询参数，包含分页及过滤条件
     * @param userId 用户ID，用于指定要查询的用户
     * @return 分页查询结果，包含带有名称信息的用户列表及分页信息
     */
    PageUtils listWithName(Map<String, Object> params, Long userId);

    /**
     * 更新审批单据信息
     * 此方法用于更新审批单据，特别是与教师相关的审批信息
     *
     * @param shenqinSingle 审批单据实体，包含要更新的审批信息
     * @return 更新操作是否成功
     */
    Boolean updateWithTeacher(ShenqinSingleEntity shenqinSingle);
}

