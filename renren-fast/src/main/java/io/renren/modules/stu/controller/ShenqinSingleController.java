package io.renren.modules.stu.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import io.renren.common.annotation.SysLog;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.stu.entity.ShenqinSingleEntity;
import io.renren.modules.stu.service.ShenqinSingleService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 申请表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:08
 */
@Slf4j
@RestController
@RequestMapping("stu/shenqinsingle")
public class ShenqinSingleController extends AbstractController {
    @Autowired
    private ShenqinSingleService shenqinSingleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("stu:shenqinsingle:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = shenqinSingleService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 先进列表 带上项目名称
     */
    @RequestMapping("/listWithName")
//    @RequiresPermissions("stu:shenqinsingle:list")
    public R listWithName(@RequestParam Map<String, Object> params){
        SysUserEntity user = getUser();
        PageUtils page = shenqinSingleService.listWithName(params, Long.valueOf(user.getUsername()));

        return R.ok().put("page", page);
    }

    /**
     * 先进列表 带上项目名称
     */
    @RequestMapping("/listWithNameForTeacher")
//    @RequiresPermissions("stu:shenqinsingle:list")
    public R listWithNameForTeacher(@RequestParam Map<String, Object> params){
//        SysUserEntity user = getUser();
        PageUtils page = shenqinSingleService.listWithNameForTeacher(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("stu:shenqinsingle:info")
    public R info(@PathVariable("id") Long id){
		ShenqinSingleEntity shenqinSingle = shenqinSingleService.getById(id);

        return R.ok().put("shenqinSingle", shenqinSingle);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("stu:shenqinsingle:save")
    public R save(@RequestBody ShenqinSingleEntity shenqinSingle){
        shenqinSingle.setTijiaoTime(new Date());
        shenqinSingle.setPass(0);
		shenqinSingleService.save(shenqinSingle);

        return R.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/updateWithTeacher")
//    @RequiresPermissions("stu:shenqinsingle:save")
    public R updateWithTeacher(@RequestBody ShenqinSingleEntity shenqinSingle){
        log();
        shenqinSingle.setTijiaoTime(new Date());
        shenqinSingleService.updateWithTeacher(shenqinSingle);

        return R.ok();
    }

    @SysLog("申报审批")
    public void log(){
        log.debug("申报审批");
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("stu:shenqinsingle:update")
    public R update(@RequestBody ShenqinSingleEntity shenqinSingle){
        shenqinSingle.setShenpiTime(new Date());
		shenqinSingleService.updateById(shenqinSingle);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("stu:shenqinsingle:delete")
    public R delete(@RequestBody Long[] ids){
		shenqinSingleService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
