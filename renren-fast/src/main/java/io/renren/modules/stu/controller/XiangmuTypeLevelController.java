package io.renren.modules.stu.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.stu.entity.XiangmuTypeLevelEntity;
import io.renren.modules.stu.service.XiangmuTypeLevelService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;


/**
 * 申请类型(level型)
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:07
 */
@RestController
@RequestMapping("stu/xiangmutypelevel")
public class XiangmuTypeLevelController {
    @Autowired
    private XiangmuTypeLevelService xiangmuTypeLevelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("stu:xiangmutypelevel:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = xiangmuTypeLevelService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("stu:xiangmutypelevel:info")
    public R info(@PathVariable("id") Long id) {
        XiangmuTypeLevelEntity xiangmuTypeLevel = xiangmuTypeLevelService.getById(id);

        return R.ok().put("xiangmuTypeLevel", xiangmuTypeLevel);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("stu:xiangmutypelevel:save")
    public R save(@RequestBody XiangmuTypeLevelEntity xiangmuTypeLevel) {
        xiangmuTypeLevelService.save(xiangmuTypeLevel);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("stu:xiangmutypelevel:update")
    public R update(@RequestBody XiangmuTypeLevelEntity xiangmuTypeLevel) {
        xiangmuTypeLevelService.updateById(xiangmuTypeLevel);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("stu:xiangmutypelevel:delete")
    public R delete(@RequestBody Long[] ids) {
        xiangmuTypeLevelService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    //示例代码

//    @RequestMapping("/list/tree")
//    public R listTree(){
//        List<CategoryEntity> list = categoryService.listTree();
//        return R.ok().put("listTree", list);
//    }
//    //生成树的核心代码 返回所有分类及其子类(带层级关系-树形图)
//    //涉及java8流式计算+递归操作
//    @Override
//    public List<CategoryEntity> listTree() {
//        //1-查询所有的分类信息
//        List<CategoryEntity> entityList = baseMapper.selectList(null); //返回数据库中所有数据
//        //2-组装为树形结构(流式计算+递归)
//        List<CategoryEntity> treeList = entityList.stream().filter(entity -> {
//            //2.1-过虑对象 返回一级分类(头节点)
//            return entity.getParentId() == 0;
//        }).map(entity -> {
//            //2.2-对一级分类进行map映射 给每个分类对应一个子分类
//            entity.setChildrenCategories(getChildrenCategories(entity, entityList));
//            return entity;
//        }).sorted((entity1, entity2) -> {
//            //2.3-排序(对一级分类的id?记进行分类)
//            return (entity1.getSort() == null ? 0 : entity1.getSort()) -
//                    (entity2.getSort() == null ? 0 : entity2.getSort());
//        }).//2.4-将处理好的数据进行转换收集到集合中
//                collect(Collectors.toList());
//
//        //2.5-返回带层级关系的树形
//        return treeList;
//    }

    /**
     * 添加流式计算代码 返回所有申请项目种类的树形结构
     */
    @RequestMapping("/list/singleTree")
    public R listTreeSingle() {
        List<XiangmuTypeLevelEntity> list = xiangmuTypeLevelService.listTreeSingle();
        return R.ok().put("listTree", list);
    }
    @RequestMapping("/list/groupTree")
    public R listTreeGroup() {
        List<XiangmuTypeLevelEntity> list = xiangmuTypeLevelService.listTreeGroup();
        return R.ok().put("listTree", list);
    }
}
