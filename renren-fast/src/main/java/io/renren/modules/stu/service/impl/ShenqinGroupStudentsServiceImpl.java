package io.renren.modules.stu.service.impl;

import io.renren.modules.stu.service.UsersService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.stu.dao.ShenqinGroupStudentsDao;
import io.renren.modules.stu.entity.ShenqinGroupStudentsEntity;
import io.renren.modules.stu.service.ShenqinGroupStudentsService;

import javax.annotation.Resource;


@Service("shenqinGroupStudentsService")
public class ShenqinGroupStudentsServiceImpl extends ServiceImpl<ShenqinGroupStudentsDao, ShenqinGroupStudentsEntity> implements ShenqinGroupStudentsService {

    @Resource
    UsersService usersService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ShenqinGroupStudentsEntity> page = this.page(
                new Query<ShenqinGroupStudentsEntity>().getPage(params),
                new QueryWrapper<ShenqinGroupStudentsEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils listByGroupId(Long groupId, Map<String, Object> params) {
        //根据groupId查询
        IPage<ShenqinGroupStudentsEntity> page = this.page(
                new Query<ShenqinGroupStudentsEntity>().getPage(params),
                new QueryWrapper<ShenqinGroupStudentsEntity>().eq("group_id",groupId)
        );
        //对Entity的name数据进行填充
        List<ShenqinGroupStudentsEntity> records = page.getRecords();
        for(int i=0;i<records.size();i++){
            ShenqinGroupStudentsEntity entity = records.get(i);
            entity.setStuName(usersService.getById(entity.getUserId()).getUsername());
            records.set(i,entity);
        }

        return new PageUtils(page);
    }
    /**
     *     @Override
     *     public PageUtils listWithNameForTeacher(Map<String, Object> params) {
     *         // 创建分页对象
     *         IPage<ShenqinSingleEntity> page = new Query<ShenqinSingleEntity>().getPage(params);
     *
     *         // 创建查询条件
     *         QueryWrapper<ShenqinSingleEntity> queryWrapper = new QueryWrapper<>();
     *
     *         // 执行分页查询
     *         IPage<ShenqinSingleEntity> resultPage = this.page(page, queryWrapper);
     *         List<ShenqinSingleEntity> records = resultPage.getRecords();
     *         for(int i=0;i<records.size();i++){
     *             ShenqinSingleEntity shenqinSingleEntity = records.get(i);
     *             XiangmuTypeLevelEntity byId = xiangmuTypeLevelService.getById(shenqinSingleEntity.getXiangmuId());
     *             shenqinSingleEntity.setXiangmuName(byId.getName());
     *             shenqinSingleEntity.setFenshuSingle(byId.getFenshuSingle());
     *             records.set(i,shenqinSingleEntity);
     *         }
     *
     *         // 返回分页结果
     *         return new PageUtils(resultPage);
     *     }
     */

}
