package io.renren.modules.stu.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.renren.modules.stu.entity.TransferEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.stu.entity.UsersEntity;
import io.renren.modules.stu.service.UsersService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 学生
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:08
 */
@RestController
@RequestMapping("stu/users")
public class UsersController {
    @Autowired
    private UsersService usersService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("stu:users:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = usersService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{userId}")
//    @RequiresPermissions("stu:users:info")
    public R info(@PathVariable("userId") Long userId){
		UsersEntity users = usersService.getById(userId);

        return R.ok().put("users", users);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("stu:users:save")
    public R save(@RequestBody UsersEntity users){
		usersService.save(users);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("stu:users:update")
    public R update(@RequestBody UsersEntity users){
		usersService.updateById(users);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("stu:users:delete")
    public R delete(@RequestBody Long[] userIds){
		usersService.removeByIds(Arrays.asList(userIds));

        return R.ok();
    }

    /**
     * 获得年级对应的人
     */
    @RequestMapping("/getUserByNianji/{nianji}")
    public R getUserByNianji(@PathVariable("nianji") String nianji){
        List<UsersEntity> userByNianji = usersService.getUserByNianji(nianji);
        ArrayList<TransferEntity> retList = new ArrayList<>();
        for(UsersEntity users : userByNianji){
            retList.add(new TransferEntity(users.getUserId(),
                    users.getUserId()+" "+users.getUsername(),false));
        }
        return R.ok().put("users",retList);
    }

}
