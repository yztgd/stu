package io.renren.modules.stu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.stu.entity.ShenqinGroupEntity;
import io.renren.modules.stu.entity.ShenqinSingleEntity;

import java.util.Map;

/**
 * 多人项目表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-18 09:20:09
 */
public interface ShenqinGroupService extends IService<ShenqinGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils listWithNameForTeacher(Map<String, Object> params);

    Boolean updateWithTeacher(ShenqinGroupEntity shenqinGroup);

}

