package io.renren.modules.stu.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.stu.entity.ShenqinGroupStudentsEntity;
import io.renren.modules.stu.service.ShenqinGroupStudentsService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 多人项目关联学生表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-18 09:20:08
 */
@RestController
@RequestMapping("stu/shenqingroupstudents")
public class ShenqinGroupStudentsController {
    @Autowired
    private ShenqinGroupStudentsService shenqinGroupStudentsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("stu:shenqingroupstudents:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = shenqinGroupStudentsService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 列表 但是根据group_id查询所有存在的小壁灯
     * 但是为什么不直接写出来呢
     * 为什么
     * 为什么
     * 为什么
     * 为什么
     */
    @RequestMapping("/list/{groupId}")
    public R listByGroupId(@PathVariable("groupId") Long groupId,
                           @RequestParam Map<String, Object> params){
        return R.ok().put("page",shenqinGroupStudentsService.listByGroupId(groupId,params));
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("stu:shenqingroupstudents:info")
    public R info(@PathVariable("id") Long id){
		ShenqinGroupStudentsEntity shenqinGroupStudents = shenqinGroupStudentsService.getById(id);

        return R.ok().put("shenqinGroupStudents", shenqinGroupStudents);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("stu:shenqingroupstudents:save")
    public R save(@RequestBody ShenqinGroupStudentsEntity shenqinGroupStudents){
		shenqinGroupStudentsService.save(shenqinGroupStudents);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("stu:shenqingroupstudents:update")
    public R update(@RequestBody ShenqinGroupStudentsEntity shenqinGroupStudents){
		shenqinGroupStudentsService.updateById(shenqinGroupStudents);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("stu:shenqingroupstudents:delete")
    public R delete(@RequestBody Long[] ids){
		shenqinGroupStudentsService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
