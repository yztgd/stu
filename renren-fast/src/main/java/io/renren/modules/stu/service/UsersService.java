package io.renren.modules.stu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.stu.entity.UsersEntity;

import java.util.List;
import java.util.Map;

/**
 * 学生
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:08
 */
public interface UsersService extends IService<UsersEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<UsersEntity> getUserByNianji(String nianji);
}

