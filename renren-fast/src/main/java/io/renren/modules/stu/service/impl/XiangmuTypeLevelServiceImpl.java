package io.renren.modules.stu.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.stu.dao.XiangmuTypeLevelDao;
import io.renren.modules.stu.entity.XiangmuTypeLevelEntity;
import io.renren.modules.stu.service.XiangmuTypeLevelService;


@Service("xiangmuTypeLevelService")
public class XiangmuTypeLevelServiceImpl extends ServiceImpl<XiangmuTypeLevelDao, XiangmuTypeLevelEntity> implements XiangmuTypeLevelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<XiangmuTypeLevelEntity> page = this.page(
                new Query<XiangmuTypeLevelEntity>().getPage(params),
                new QueryWrapper<XiangmuTypeLevelEntity>()
        );

        return new PageUtils(page);
    }

    //生成树的核心代码 返回所有分类及其子类(带层级关系-树形图)
    //涉及java8流式计算+递归操作
    @Override
    public List<XiangmuTypeLevelEntity> listTreeSingle() {
        //1-查询所有的分类信息
        List<XiangmuTypeLevelEntity> entityList = baseMapper.selectList(null); //返回数据库中所有数据
        ArrayList<XiangmuTypeLevelEntity> xiangmuTypeLevelEntities = new ArrayList<>();
        for (XiangmuTypeLevelEntity entity : entityList) {
            if(entity.getRenType()==1) { //项目人数等于1为单人项目
                xiangmuTypeLevelEntities.add(entity);
            }
        }

        //2-组装为树形结构(流式计算+递归)
        List<XiangmuTypeLevelEntity> treeList = xiangmuTypeLevelEntities.stream().filter(entity -> {
            //2.1-过虑对象 返回一级分类(头节点)
            return entity.getParentId() == 0;
        }).map(entity -> {
            //2.2-对一级分类进行map映射 给每个分类对应一个子分类
            entity.setChildrenCategories(getChildrenCategories(entity, entityList));
            return entity;
        }).//2.4-将处理好的数据进行转换收集到集合中
                collect(Collectors.toList());

        //2.5-返回带层级关系的树形
        return treeList;
    }

    @Override
    public List<XiangmuTypeLevelEntity> listTreeGroup() {
        //1-查询所有的分类信息
        List<XiangmuTypeLevelEntity> entityList = baseMapper.selectList(null); //返回数据库中所有数据
        ArrayList<XiangmuTypeLevelEntity> xiangmuTypeLevelEntities = new ArrayList<>();
        for (XiangmuTypeLevelEntity entity : entityList) {
            if(entity.getRenType()>1) { //项目人数大于1为多人(团队)项目
                xiangmuTypeLevelEntities.add(entity);
            }
        }

        //2-组装为树形结构(流式计算+递归)
        List<XiangmuTypeLevelEntity> treeList = xiangmuTypeLevelEntities.stream().filter(entity -> {
            //2.1-过虑对象 返回一级分类(头节点)
            return entity.getParentId() == 0;
        }).map(entity -> {
            //2.2-对一级分类进行map映射 给每个分类对应一个子分类
            entity.setChildrenCategories(getChildrenCategories(entity, entityList));
            return entity;
        }).//2.4-将处理好的数据进行转换收集到集合中
                collect(Collectors.toList());

        //2.5-返回带层级关系的树形
        return treeList;
    }

    /**
     * 返回CascadedCategoryId需要用到递归去寻找当前所在的层级
     * 递归查找parentId
     * @param id
     * @return 数组 形式: [1,21,301]
     */
    public Long[] getCascadedCategoryId(Long id) {
        //1.创建集合 收集层级关系
        ArrayList<Long> cascadedCategoryId = new ArrayList<>();
        //2.调用方法进行处理 递归
        getParentCategoryId(id,cascadedCategoryId);
        //3.集合反转
        Collections.reverse(cascadedCategoryId);
        return cascadedCategoryId.toArray(new Long[cascadedCategoryId.size()]);
    }

    /**
     * 编写方法 根据categoryId查找层级关系 例如根据二级id301查找到一级和顶级id
     */
    private List<Long> getParentCategoryId(Long categoryId,List<Long> cascadedCategoryIds) {
        // 比较麻烦
        //1 先把categoryId放入list
        cascadedCategoryIds.add(categoryId);
        //2 根据categoryId得到对应的CategoryEntity
        XiangmuTypeLevelEntity categoryEntity = this.getById(categoryId);
        //3 判断categoryEntity的parentId是否为0 用于判断他是否还有上级
        if(categoryEntity.getParentId()!=0){
            //递归查找
            getParentCategoryId(categoryEntity.getParentId(),cascadedCategoryIds);
        }

        return cascadedCategoryIds;
    }

    /**
     * 递归方法 用于上文方法2.2的需求 返回目标分类的所有子分类
     */
    private List<XiangmuTypeLevelEntity> getChildrenCategories(
            XiangmuTypeLevelEntity root, //当前父类
            List<XiangmuTypeLevelEntity> all //所有分类
    ) {
        //过虑
        return all.stream().filter(categoryEntity -> {
            //此处会产生问题 该处==两端均为long包装类型的返回值
            return categoryEntity.getParentId().longValue() == root.getId().longValue();
        }).map(categoryEntity -> {
            //找到子分类并递归
            categoryEntity.setChildrenCategories(getChildrenCategories(categoryEntity,all));
            return categoryEntity;
        }).collect(Collectors.toList());
    }

}
