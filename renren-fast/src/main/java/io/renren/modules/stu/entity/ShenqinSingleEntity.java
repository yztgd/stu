package io.renren.modules.stu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 申请表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:08
 */
@Data
@TableName("stu_shenqin_single")
public class ShenqinSingleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 申请表id
	 */
	@TableId
	private Long id;
	/**
	 * 申请人学号
	 */
	private Long userId;
	/**
	 * 申请内容
	 */
	private String txt;
	/**
	 * 申请材料地址
	 */
	private String ossUrl;
	/**
	 * 提交时间
	 */
	private Date tijiaoTime;
	/**
	 * 审批时间
	 */
	private Date shenpiTime;
	/**
	 * 审批人
	 */
	private String shenpiUser;
	/**
	 * 是否通过1：通过 0：未通过/待通过
	 */
	private Integer pass;
	/**
	 * 项目类型id
	 */
	private Long xiangmuId;

	/**
	 * 项目名称 来源于根据项目Id查询
	 */
	@TableField(exist=false) //表示该属性不对应表属性 不会在查询时进行自动装配
	private String xiangmuName;

	/**
	 * 项目分值(单人)
	 */
	@TableField(exist=false)
	private Integer fenshuSingle;

}
