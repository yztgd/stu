package io.renren.modules.stu.service.impl;

import io.renren.modules.stu.entity.XiangmuTypeLevelEntity;
import io.renren.modules.stu.service.XiangmuTypeLevelService;
import io.renren.modules.sys.entity.SysLogEntity;
import io.renren.modules.sys.service.SysLogService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.stu.dao.ShenqinSingleDao;
import io.renren.modules.stu.entity.ShenqinSingleEntity;
import io.renren.modules.stu.service.ShenqinSingleService;

import javax.annotation.Resource;


@Service("shenqinSingleService")
public class ShenqinSingleServiceImpl extends ServiceImpl<ShenqinSingleDao, ShenqinSingleEntity> implements ShenqinSingleService {
    @Resource
    XiangmuTypeLevelService xiangmuTypeLevelService;
    @Resource
    SysLogService sysLogService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ShenqinSingleEntity> page = this.page(
                new Query<ShenqinSingleEntity>().getPage(params),
                new QueryWrapper<ShenqinSingleEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils pageByUserId(Map<String, Object> params, Long userId) {
        // 创建分页对象
        IPage<ShenqinSingleEntity> page = new Query<ShenqinSingleEntity>().getPage(params);

        // 创建查询条件
        QueryWrapper<ShenqinSingleEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);

        // 执行分页查询
        IPage<ShenqinSingleEntity> resultPage = this.page(page, queryWrapper);

        // 返回分页结果
        return new PageUtils(resultPage);
    }

    @Override
    public PageUtils listWithNameForTeacher(Map<String, Object> params) {
        // 创建分页对象
        IPage<ShenqinSingleEntity> page = new Query<ShenqinSingleEntity>().getPage(params);

        // 创建查询条件
        QueryWrapper<ShenqinSingleEntity> queryWrapper = new QueryWrapper<>();

        // 执行分页查询
        IPage<ShenqinSingleEntity> resultPage = this.page(page, queryWrapper);
        List<ShenqinSingleEntity> records = resultPage.getRecords();
        for(int i=0;i<records.size();i++){
            ShenqinSingleEntity shenqinSingleEntity = records.get(i);
            XiangmuTypeLevelEntity byId = xiangmuTypeLevelService.getById(shenqinSingleEntity.getXiangmuId());
            shenqinSingleEntity.setXiangmuName(byId.getName());
            shenqinSingleEntity.setFenshuSingle(byId.getFenshuSingle());
            records.set(i,shenqinSingleEntity);
        }

        // 返回分页结果
        return new PageUtils(resultPage);
    }

    @Override
    public PageUtils listWithName(Map<String, Object> params,Long userId) {
        // 创建分页对象
        IPage<ShenqinSingleEntity> page = new Query<ShenqinSingleEntity>().getPage(params);

        // 创建查询条件
        QueryWrapper<ShenqinSingleEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);

        // 执行分页查询
        IPage<ShenqinSingleEntity> resultPage = this.page(page, queryWrapper);

        List<ShenqinSingleEntity> records = resultPage.getRecords();
        for(int i=0;i<records.size();i++){
            ShenqinSingleEntity shenqinSingleEntity = records.get(i);
            XiangmuTypeLevelEntity byId = xiangmuTypeLevelService.getById(shenqinSingleEntity.getXiangmuId());
            shenqinSingleEntity.setXiangmuName(byId.getName());
            shenqinSingleEntity.setFenshuSingle(byId.getFenshuSingle());
            records.set(i,shenqinSingleEntity);
        }
        System.out.println(page);
        return new PageUtils(page);
    }

    /**
     * 修改 使得审批人更新
     * @param shenqinSingle
     * @return
     */
    @Override
    public Boolean updateWithTeacher(ShenqinSingleEntity shenqinSingle) {
//        List<SysLogEntity> list = sysLogService.list();
        int count = sysLogService.count();
//        String username = list.get(list.size() - 1).getUsername();
        String username = sysLogService.getById(count).getUsername();
        shenqinSingle.setShenpiUser(username);
        boolean b = this.updateById(shenqinSingle);
        return b;
    }


}
