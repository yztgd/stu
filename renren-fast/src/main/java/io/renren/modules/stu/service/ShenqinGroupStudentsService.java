package io.renren.modules.stu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.stu.entity.ShenqinGroupStudentsEntity;

import java.util.Map;

/**
 * 多人项目关联学生表
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-18 09:20:08
 */
public interface ShenqinGroupStudentsService extends IService<ShenqinGroupStudentsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils listByGroupId(Long groupId,Map<String, Object> params);
}

