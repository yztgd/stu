package io.renren.modules.stu.dao;

import io.renren.modules.stu.entity.ShenqinGroupStudentsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 多人项目关联学生表
 * 
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-18 09:20:08
 */
@Mapper
public interface ShenqinGroupStudentsDao extends BaseMapper<ShenqinGroupStudentsEntity> {
	
}
