package io.renren.modules.stu.dao;

import io.renren.modules.stu.entity.XiangmuTypeLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 申请类型(level型)
 * 
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:07
 */
@Mapper
public interface XiangmuTypeLevelDao extends BaseMapper<XiangmuTypeLevelEntity> {
	
}
