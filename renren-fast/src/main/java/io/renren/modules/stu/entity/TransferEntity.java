package io.renren.modules.stu.entity;

import lombok.*;

/**
 * @author LINNIN
 * @name stu
 * @time 2024/9/19 10:31
 * @note
 * @userfor
 * @out
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferEntity {
    private Long key;
    private String label;
    private Boolean disabled;
}
