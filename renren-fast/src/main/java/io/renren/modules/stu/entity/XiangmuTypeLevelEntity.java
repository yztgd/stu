package io.renren.modules.stu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 申请类型(level型)
 *
 * @author linnin
 * @email linnin233@163.com
 * @date 2024-09-14 11:13:07
 */
@Data
@TableName("stu_xiangmu_type_level")
public class XiangmuTypeLevelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 项目类型id
	 */
	@TableId
	private Long id;
	/**
	 * 项目名称
	 */
	private String name;
	/**
	 * 是否为可申请项目0：不可申请  1：可申请
	 */
	private Integer useType;
	/**
	 * 申请类型0单人1多人
	 */
	private Integer renType;
	/**
	 * 父级id
	 */
	private Long parentId;
	/**
	 * 层级
	 */
	private Integer level;
	/**
	 * 单人项目分数
	 */
	private Integer fenshuSingle;
	/**
	 * 多人项目总分数
	 */
	private Integer fenshuGroup;
	/**
	 * 多人项目主持分数
	 */
	private Integer fenshuZhuchi;
	/**
	 * 层级属性 子对象集合
	 * 未对应表字段
	 */
	@JsonInclude(JsonInclude.Include.NON_EMPTY) //如果为空数组不返回
	@TableField(exist=false) //表示该属性不对应表属性 不会在查询时进行自动装配
	private List<XiangmuTypeLevelEntity> childrenCategories;

}
