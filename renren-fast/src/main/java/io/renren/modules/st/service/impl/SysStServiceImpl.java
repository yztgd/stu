/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.st.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.st.dao.SysStDao;
import io.renren.modules.st.entity.SysStEntity;
import io.renren.modules.st.service.SysStService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("sysStService")
public class SysStServiceImpl extends ServiceImpl<SysStDao, SysStEntity> implements SysStService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<SysStEntity> page = this.page(
			new Query<SysStEntity>().getPage(params)
		);

		return new PageUtils(page);
	}

}
