/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.st.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.st.entity.SysStEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysStDao extends BaseMapper<SysStEntity> {

}
